/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.cros.lab3;

/**
 *
 * @author informatics
 */
import java.util.Scanner;

/**
 *
 * @author informatics
 */
import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab3 {
    static char[][] Table = {{'_','_','_'},
        {'_','_','_'},
        {'_','_','_'}
    };
    static char currentPlayer = 'X';
    public static int row;
    public static int col;
    static void printWelcome(){
        System.out.println("Welcome to XO Game!");
    
    }static void printPlayer1(){
        System.out.println("Player 1: X");
    
    }static void printPlayer2(){
        System.out.println("Player 2: O");
    }
    
    static void printTable(){
        for(int i=0;i<3;i++){
            
        for(int j=0;j<3;j++){
            System.out.print(Table[i][j]+" ");
        }
            System.out.println("");
       }      
     }
    static void printTurn(){
        System.out.println(currentPlayer+" Turn");
    }
    static void printRowCol(){
        Scanner kb = new Scanner(System.in);
        while(true){
         System.out.print("Input Row,Col :");
        row = kb.nextInt();
        col = kb.nextInt();
        if(Table[row-1][col-1] == '_'){
           Table[row-1][col-1]=currentPlayer; 
           break;
        }
      } //System.out.println(""+row+""+col);
    }
    static void switchPlayer(){
        if(currentPlayer == 'X'){
            currentPlayer = 'O';
        }else{
            currentPlayer = 'X';
        }
    }
    static boolean checkWin(char[][] Table){
        return checkRowWin(Table) || checkColWin(Table) || checkDiagonalWin(Table);
    }
    static boolean checkRowWin(char[][] Table){
        for (int i = 0; i < 3; i++) {
            if (Table[i][0] != '_' && Table[i][0] == Table[i][1] && Table[i][1] == Table[i][2]) {
                return true;
            }
        }
        return false;
    }
    static boolean checkColWin(char[][] Table){
        for (int i = 0; i < 3; i++) {
            if (Table[0][i] != '_' && Table[0][i] == Table[1][i] && Table[1][i] == Table[2][i]) {
                return true;
            }
        }
        return false;

    }
    static boolean checkDiagonalWin(char[][] Table){
        return (Table[0][0] != '_' && Table[0][0] == Table[1][1] && Table[1][1] == Table[2][2]) ||
                (Table[0][2] != '_' && Table[0][2] == Table[1][1] && Table[1][1] == Table[2][0]);

    }
    static boolean checkDraw(char[][] Table){
         for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (Table[i][j] == '_') {
                    return false;
                }
            }
        }
        return true;

    }
    static void resetGame(){
        Table = new char[][]{{'_', '_', '_'}, {'_', '_', '_'}, {'_', '_', '_'}};
        currentPlayer = 'X';
    }  
    static void printWin(){
        System.out.println(currentPlayer+" Win!!");
    }
    static void printDraw(){
        System.out.println("Draw!!");
    }
    public static void main(String[] args) {
        printWelcome();
        printPlayer1();
        printPlayer2();
        boolean continueGame = true;
        while (continueGame) {
        while(true){
            printTable();
            printTurn();
            printRowCol();
            if(checkWin(Table)){
                printTable();
                printWin();
                break;
            }
            if(checkDraw(Table)){
                printTable();
                printDraw();
                break;
            }
            switchPlayer();
            }
            System.out.print("Do you want to continue playing? (Y/N): ");
            Scanner sc = new Scanner(System.in);
            String input = sc.next();
            char character = input.charAt(0);
        
            switch (character) {
            case 'Y':
            case 'y':
                resetGame();
                break;
            case 'N':
            case 'n':
                continueGame = false;
                break;
            default:
                resetGame();
                break;
            }
        }
    } 
}