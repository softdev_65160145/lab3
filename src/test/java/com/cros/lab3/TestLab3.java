package com.cros.lab3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import com.cros.lab3.Lab3;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class TestLab3 {
    
    public TestLab3() {
    }

    
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWin_O_Verticel1_output_true(){
        char [][] Table = {{'O','O','O'}
                          ,{'_','_','_'}
                          ,{'_','_','_'}};
        boolean result = Lab3.checkWin(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Verticel2_output_true(){
        char [][] Table = {{'_','_','_'}
                          ,{'O','O','O'}
                          ,{'_','_','_'}};
        boolean result = Lab3.checkWin(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Verticel3_output_true(){
        char [][] Table = {{'_','_','_'}
                          ,{'_','_','_'}
                          ,{'O','O','O'}};
        boolean result = Lab3.checkWin(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Verticel4_output_true(){
        char [][] Table = {{'O','_','_'}
                          ,{'O','_','_'},
                           {'O','_','_'}};
        boolean result = Lab3.checkWin(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Verticel5_output_true(){
        char [][] Table = {{'_','O','_'}
                          ,{'_','O','_'},
                           {'_','O','_'}};
        boolean result = Lab3.checkWin(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Verticel6_output_true(){
        char [][] Table = {{'_','_','O'}
                          ,{'_','_','O'},
                           {'_','_','O'}};
        boolean result = Lab3.checkWin(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Verticel7_output_true(){
        char [][] Table = {{'O','_','_'}
                          ,{'_','O','_'},
                           {'_','_','O'}};
        boolean result = Lab3.checkWin(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Verticel8_output_true(){
        char [][] Table = {{'_','_','O'}
                          ,{'_','O','_'},
                           {'O','_','_'}};
        boolean result = Lab3.checkWin(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Verticel1_output_true(){
        char [][] Table = {{'X','X','X'}
                          ,{'_','_','_'}
                          ,{'_','_','_'}};
        boolean result = Lab3.checkWin(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Verticel2_output_true(){
        char [][] Table = {{'_','_','_'}
                          ,{'X','X','X'}
                          ,{'_','_','_'}};
        boolean result = Lab3.checkWin(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Verticel3_output_true(){
        char [][] Table = {{'_','_','_'}
                          ,{'_','_','_'}
                          ,{'X','X','X'}};
        boolean result = Lab3.checkWin(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Verticel4_output_true(){
        char [][] Table = {{'X','_','_'}
                          ,{'X','_','_'},
                           {'X','_','_'}};
        boolean result = Lab3.checkWin(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Verticel5_output_true(){
        char [][] Table = {{'_','X','_'}
                          ,{'_','X','_'},
                           {'_','X','_'}};
        boolean result = Lab3.checkWin(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Verticel6_output_true(){
        char [][] Table = {{'_','_','X'}
                          ,{'_','_','X'},
                           {'_','_','X'}};
        boolean result = Lab3.checkWin(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Verticel7_output_true(){
        char [][] Table = {{'X','_','_'}
                          ,{'_','X','_'},
                           {'_','_','X'}};
        boolean result = Lab3.checkWin(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Verticel8_output_true(){
        char [][] Table = {{'_','_','X'}
                          ,{'_','X','_'},
                           {'X','_','_'}};
        boolean result = Lab3.checkWin(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckDraw_Verticel1_output_true(){
        char [][] Table = {{'O','X','O'}
                          ,{'O','X','X'},
                           {'X','O','X'}};
        boolean result = Lab3.checkDraw(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckDraw_Verticel2_output_true(){
        char [][] Table = {{'X','O','O'}
                          ,{'O','X','X'},
                           {'X','X','O'}};
        boolean result = Lab3.checkDraw(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckDraw_Verticel3_output_true(){
        char [][] Table = {{'X','X','O'}
                          ,{'O','O','X'},
                           {'X','O','X'}};
        boolean result = Lab3.checkDraw(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckDraw_Verticel4_output_true(){
        char [][] Table = {{'X','X','O'}
                          ,{'O','O','X'},
                           {'X','X','O'}};
        boolean result = Lab3.checkDraw(Table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckDraw_Verticel5_output_true(){
        char [][] Table = {{'O','X','O'}
                          ,{'X','O','X'},
                           {'X','O','X'}};
        boolean result = Lab3.checkDraw(Table);
        assertEquals(true,result);
    }
}
